import sequtils

type
  SeqMap*[A, B] = ref object
    a: seq[A]
    b: seq[B]

proc newSeqMap*[A, B](): SeqMap[A, B] =
  return SeqMap[A, B]()

proc add*[A, B](seqmap: SeqMap[A, B], a: A, b: B) =
  seqmap.a.add(a)
  seqmap.b.add(b)

proc getKey*[A, B](seqmap: SeqMap[A, B], i: int): A =
  return seqmap.a[i]

proc getValue*[A, B](seqmap: SeqMap[A, B], i: int): B =
  return seqmap.b[i]

proc findByKey*[A, B](seqmap: SeqMap[A, B], key: A): int =
  return seqmap.a.find(key)

proc findByValue*[A, B](seqmap: SeqMap[A, B], val: B): int =
  return seqmap.b.find(val)

proc valueByKey*[A, B](seqmap: SeqMap[A, B], key: A): B =
  return seqmap.b[seqmap.a.find(key)]

proc keyByValue*[A, B](seqmap: SeqMap[A, B], val: B): A =
  return seqmap.a[seqmap.b.find(val)]

template ContainsAllKeys*[A, B](seqmap: SeqMap[A, B], criterion: openArray[A]): bool =
  for item in seqmap.a.items:
    for key in criterion:
      if item != key: 
        return false

  return true

template ContainsAllValues*[A, B](seqmap: SeqMap[A, B], criterion: openArray[B]): bool =
  for item in seqmap.b.items:
    for key in criterion:
      if item != key: 
        return false

  return true
