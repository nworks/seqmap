# Package

version       = "0.1.0"
author        = "Matthew Murray"
description   = "Some kind of associative sequence."
license       = "MIT"


# Dependencies

requires "nim >= 0.19.9"
